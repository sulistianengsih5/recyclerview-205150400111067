package com.example.recycleview205150400111067;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, RecyclerViewInterface {

    RecyclerView rv;
    MahasiswaAdapter adapter;
    public static String TAG = "RV1";
    ArrayList<Mahasiswa> data;
    Button btnSimpan;
    TextView fieldNim, fieldNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fieldNim = findViewById(R.id.fieldNIM);
        fieldNama = findViewById(R.id.fieldNama);
        btnSimpan = findViewById(R.id.btSimpan);
        btnSimpan.setOnClickListener(this::onClick);

        loadData();
        initRecyclerView();
    }

    private void initRecyclerView() {
        rv = findViewById(R.id.rvMahasiswa);
        adapter = new MahasiswaAdapter(this, data, this);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));
    }

    public void addData() {
        data.add(new Mahasiswa(fieldNim.getText().toString(), fieldNama.getText().toString()));
    }

    public void loadData() {
        data = new ArrayList<>();
        data.add(new Mahasiswa("205150400111067", "Eden"));
        data.add(new Mahasiswa("205150400111023", "Yusuf"));
        data.add(new Mahasiswa("205150407111060", "Eve"));
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==btnSimpan.getId()) {
            addData();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(int position) {
        Log.d(TAG, "onMahasiswaClick: clicked.");
        Intent intent = new Intent(this, Activity2.class);
        String nama = data.get(position).nama;
        String nim = data.get(position).nim;

        intent.putExtra("nama", nama);
        intent.putExtra("nim", nim);
        startActivity(intent);
    }
}