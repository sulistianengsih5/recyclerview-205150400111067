package com.example.recycleview205150400111067;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView tvNim2, tvNama2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        tvNim2 = findViewById(R.id.tvNim2);
        tvNama2 = findViewById(R.id.tvNama2);

        Intent intent = getIntent();
        String nama = getIntent().getStringExtra("nama");
        String nim = getIntent().getStringExtra("nim");
        tvNim2.setText("NIM : " + nim);
        tvNama2.setText("Nama : " + nama);
    }
}