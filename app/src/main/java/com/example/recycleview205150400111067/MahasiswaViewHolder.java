package com.example.recycleview205150400111067;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView txNama;
    TextView txNim;
    ImageView foto;
    Context context;
    RecyclerViewInterface rvi;

    public MahasiswaViewHolder(@NonNull View itemView, RecyclerViewInterface rvi) {
        super(itemView);
        this.context = itemView.getContext();
        this.txNama = itemView.findViewById(R.id.tvNama);
        this.txNim = itemView.findViewById(R.id.tvNim);
        this.foto = itemView.findViewById((R.id.imageView));
        this.rvi = rvi;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
       rvi.onItemClick(getAdapterPosition());
    }
}
